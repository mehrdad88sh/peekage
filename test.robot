*** Settings ***
Documentation
Test Setup                            Run Keywords                              Open Chrome Browser
Test Teardown                         Close Browser
Library                               SeleniumLibrary                           timeout=5s
Library                               Collections
Library                               String
Library                               ../lib/convert.py

*** Variables ***
${max_price_selector}                 //*[contains(text(), 'گران‌ترین')]
${max_price_is_selected}              //div[6][@class="pointer text-no-wrap text-body2-strong color-primary-700 sortAndStatsHeader_SortAndStatsHeader__SortOption__yqiM8"]
${max_price_is_not_selected}          //div[6][@class="pointer text-no-wrap text-body-2 color-500 sortAndStatsHeader_SortAndStatsHeader__SortOption__yqiM8"]
${sort_button}                        css:nav.serp-sorting.popover-parent.pull-right
${price_values}                       //div[@id="plpLayoutContainer"]//div[@class="d-flex ai-center jc-end gap-1 color-700 color-400 text-h5 grow-1"]/span

*** Test Cases ***
Sort Laptops With Max Price
  Wait Until Page Contains Element    ${max_price_is_not_selected}              timeout=5s
  Click Element                       ${max_price_selector}
  Wait Until Page Contains Element    ${max_price_is_selected}                  timeout=5s
  Execute JavaScript                  window.scrollTo(0,850)
  Sleep    2s
  ${Prices}                           Create List
  ${Max_prices}                       Get Text                                  (//div[@id="plpLayoutContainer"]//div[@class="d-flex ai-center jc-end gap-1 color-700 color-400 text-h5 grow-1"]/span)[1]
  ${Max_prices}                       convert number                            ${Max_prices}
  FOR    ${i}    IN RANGE   10
         ${Elements}                  Get WebElements                           ${price_values}
         ${Price}                     Get Text                                  ${Elements}[${i}]
         ${EN_prices}                 convert number                            ${Price}
         Log To Console               ${Max_prices}
         ${Price}                     Run Keyword If                            '${EN_prices}'<='${Max_prices}'
         ...                          Convert To String                         ${EN_prices}
         ...                          ELSE     Fail       Sort Price has error
         ${Max_prices}                Convert To String                         ${Price}
         Append to List               ${Prices}                                 ${Price}
  END

*** Keywords ***
Open Chrome Browser
  Set Log Level                       trace
  Open Browser                        chrome://version                          browser=chrome
  Maximize Browser Window
  Go To                               https://www.digikala.com/search/category-notebook-netbook-ultrabook/
  Wait Until Page Contains            جدیدترین مدل‌های لپ تاپ                    timeout=5s
